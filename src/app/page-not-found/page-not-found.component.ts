import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rkcr-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['../styles/page-not-found.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
