import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'rkcr-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['../../styles/style-navbar.css']
})
export class NavBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).on('scroll',function() {
      if ($(document).scrollTop()) {
         $('nav').addClass('black');
      } else {
        $('nav').removeClass('black');
      }
    });

    $(document).ready(function(){
      $('.toggle').click(function() {
        $('.toggle').toggleClass('active');
        $('nav').toggleClass('look');
      });
    });
  }

}
